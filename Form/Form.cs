﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace Core.Forms
{
    public class FormBase
    {

        [JsonProperty("clearance")]
        public int Clearance { get; set; } = 0;
        [JsonProperty("requiredlabel")]
        public string RequiredLabel { get; set; } = "verplicht";
        [JsonProperty("postlabel")]
        public string PostLabel { get; set; } = "Verstuur";
        [JsonProperty("reference")]
        public string Reference { get; set; } = "";
        [JsonProperty("isconcept")]
        public bool IsConcept { get; set; } = true;
        [JsonProperty("isworkflow")]
        public bool IsWorkflow { get; set; } = false;
        [JsonProperty("endpoint")]
        public string Endpoint { get; set; } = "";
        [JsonProperty("post_failed_message")]
        public string PostFailedMessage { get; set; } = "The server did not respond or encountered an error";
        [JsonProperty("index")]
        public string Index { get; set; } = "forms";
        [JsonProperty("type")]
        public string Type { get; set; } = "form";
        [JsonProperty("states")]
        public States States { get; set; } = new States();
        [JsonProperty("state")]
        public string State { get; set; } = "";
        [JsonProperty("teams")]
        public Teams Teams { get; set; } = new Teams();
        [JsonProperty("team")]
        public string Team { get; set; } = "";
        [JsonProperty("user")]
        public string User { get; set; } = "";
        [JsonProperty("errors")]
        public Errors Errors { get; set; } = new Errors();
        [JsonProperty("acl")]
        public ACL ACL { get; set; } = new ACL();
        [JsonProperty("distribution")]
        public Distribution Distribution { get; set; } = new Distribution();
        [JsonProperty("created")]
        public string Created { get; set; } = "";
        [JsonProperty("modified")]
        public string Modified { get; set; } = "";
        [JsonProperty("showmarkers")]
        public bool ShowMarkers { get; set; } = false;
        [JsonProperty("title")]
        public string Title { get; set; } = "BusinessForensics Form";
        [JsonProperty("milestonelabel")]
        public string MilestoneLabel { get; set; } = "Milestone";
        [JsonProperty("transitionlabel")]
        public string TransitionLabel { get; set; } = "Please select the next milestone";
        [JsonProperty("righttoleft")]
        public bool RightToLeft { get; set; } = false;
        [JsonProperty("showoutline")]
        public bool ShowOutline { get; set; } = true;
        [JsonProperty("attachment")]
        public Attachment Attachment { get; set; } = new Attachment();
        [JsonProperty("maxfilesize")]
        public int MaxFileSize { get; set; } = 0;
        [JsonProperty("sections")]
        public Sections Sections { get; set; } = new Sections();
        //create a dictionary for fields that are not in the specification of Forensic Forms
        //these fields may be very important to control the flow in a form.
        //in the future we'll need to specify some flow control values.
        [JsonProperty("unspecified")]
        public List<string> Unspecified { get; set; } = new List<string>();
        //Multi purpose dictionary for storing unspecified items. See Unspecified for a list of names
        public Dictionary<string, string> Properties { get; set; } = new Dictionary<string, string>();
        //Create a dictionary for keys that may be hidden in different section rows.
        //This dictionary should be filled automatically, so a programmer can easily find them
        public Dictionary<string, string> Keys { get; set; } = new Dictionary<string, string>();
        //this must be called 

        public void Clean(string jsonstring)
        {
            try
            {
                ErrorMessage = "";
                //if this doesn't work then the json is bad, but it's no reason to generate an exception.
                //the job should continue.

                Newtonsoft.Json.Linq.JObject json = JObject.Parse(jsonstring);
                //these fields are added to the json, but not part of the spec.
                foreach (string reference in Unspecified)
                {

                    if (json.ContainsKey(reference))
                    {
                        Properties[reference] = json[reference].ToString();
                    }
                }
                Keys.Clear();
                //we can't extract keys from repeating groups. Keys are meant to map to contracts, customer id's

                foreach (Section s in Sections)
                {
                    foreach (Column c in s.Columns)
                    {
                        if (c.IsKey && s.Rows.Count > 0 && s.Rows[0].ContainsKey(c.Reference))
                        {
                            Keys[c.Reference] = s.Rows[0][c.Reference].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }

        }
        public string ErrorMessage { get; set; } = "";
        public string GetProperty(string fieldname)
        {
            if (Properties.ContainsKey(fieldname))
            {
                return Properties[fieldname];
            }
            return "?";

        }
        public List<string> Find(string fieldname)
        {
            List<string> result = new List<string>();
            foreach (Section s in Sections)
            {
                foreach (Dictionary<string, object> row in s.Rows)
                {
                    if (row.ContainsKey(fieldname))
                    {
                        result.Add(row[fieldname].ToString());
                    }
                }
            }
            return result;
        }
        //this method creates a document style summary in plain text.
        //but the problem here is that some of the rows will contain codes.
        //fortunately we can use the columns in the sections to find the human readable description too :).

        public string Body
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine(Title);
                builder.AppendLine("");
                foreach (string key in Properties.Keys)
                {
                    builder.AppendLine(key + ": " + Properties[key]);
                }

                foreach (Section s in Sections)
                {
                    //we don't need to show invisible sections.
                    if (!s.IsVisible) continue;
                    builder.AppendLine("");
                    builder.AppendLine(s.Name);
                    builder.AppendLine("");
                    foreach (Dictionary<string, object> row in s.Rows)
                    {
                        foreach (string key in row.Keys)
                        {

                            builder.AppendLine(key + ": " + s.DisplayValue(key, row[key].ToString()));

                        }
                    }
                }
                return builder.ToString();
            }
        }


    }
    //placeholders for something fancy
    public class States : List<string> { }
    public class Teams : List<string> { }
    public class Errors : List<string> { }
    public class ACL : List<string> { }
    public class Distribution : List<string> { }
    public class Attachment
    {
        [JsonProperty("name")]
        public string Name { get; set; } = "";
        [JsonProperty("size")]
        public int Size { get; set; } = 0;
        [JsonProperty("type")]
        public string Type { get; set; } = "";
        [JsonProperty("section_reference")]
        public string SectionReference { get; set; } = "";
        [JsonProperty("column_reference")]
        public string ColumnReference { get; set; } = "";
        [JsonProperty("modified")]
        public string Modified { get; set; } = "";



    }
    public class Sections : List<Section>
    {
        public Section this[string reference]
        {
            get
            {
                reference = reference.ToUpperInvariant();
                foreach (Section s in this.ToList())
                {
                    if (s.Reference.ToUpperInvariant() == reference) return s;
                }
                return null;
            }
        }
    }
    public class Columns : List<Column>
    {
        public Column this[string fieldname]
        {
            get
            {
                fieldname = fieldname.ToUpperInvariant();
                foreach (Column c in this.ToList())
                {
                    if (c.Reference.ToUpperInvariant() == fieldname) return c;
                }
                return null;
            }
        }
    }
    public class Section
    {
        [JsonProperty("id")]
        public int ID { get; set; } = 0;
        [JsonProperty("reference")]
        public string Reference { get; set; } = "";
        [JsonProperty("name")]
        public string Name { get; set; } = "";
        [JsonProperty("condition")]
        public string Condition { get; set; } = "";
        [JsonProperty("isvisible")]
        public bool IsVisible { get; set; } = true;
        [JsonProperty("description")]
        public string Description { get; set; } = "";
        [JsonProperty("maxrows")]
        public int MaxRows { get; set; } = 1;
        [JsonProperty("rows")]
        public List<Dictionary<string, object>> Rows { get; set; } = new List<Dictionary<string, object>>();
        [JsonProperty("rowindex")]
        public int RowIndex { get; set; } = 0;
        [JsonProperty("style")]
        public string Style { get; set; } = "section-black";
        [JsonProperty("isexpanded")]
        public bool IsExpanded { get; set; } = true;
        [JsonProperty("imagename")]
        public string ImageName { get; set; } = "";
        [JsonProperty("w7")]
        public int W7 { get; set; } = 0;
        [JsonProperty("columns")]
        public Columns Columns { get; set; } = new Columns();
        public override string ToString()
        {
            if (IsVisible) return Reference;
            return "-";
        }
        //does not work for a set of rows.
        //but most sections do not contain groups of rows.
        public string Find(string fieldname)
        {
            return RowFind(fieldname, 0);
        }
        //use this one for more than one row.
        public string RowFind(string fieldname, int rowindex)
        {
            if (rowindex >= Rows.Count) return null;
            Dictionary<string, object> row = Rows[rowindex];
            if (row.ContainsKey(fieldname))
            {
                return row[fieldname].ToString();
            }
            return string.Empty;
        }
        public string DisplayValue(string key)
        {
            //comboboxes which are important for form logic are keyvalue pairs. We need to find the 
            //pretty description.
            Column c = Columns[key];
            if (Rows[0].ContainsKey(key))
            {
                string reference = Rows[0][key].ToString();
                if (c.Component == "switch" || c.Component == "select") //todo: create enum
                {
                    Option o = c.Options[reference];
                    if (o != null) return o.Name;
                }
                return reference;
            }
            return "";
        }
        public string DisplayValue(string key, object value)
        {
            //comboboxes which are important for form logic are keyvalue pairs. We need to find the 
            //pretty description.
            Column c = Columns[key];
            if (c.Component == "switch" || c.Component == "select") //todo: create enum
            {
                string reference = value.ToString();
                Option o = c.Options[reference];
                if (o != null) return o.Name;
                return reference;
            }
            return value.ToString();

        }
    }
    public class Column
    {
        [JsonProperty("reference")]
        public string Reference { get; set; } = "";
        [JsonProperty("label")]
        public string Label { get; set; } = "";
        [JsonProperty("w7")]
        public int W7 { get; set; } = 0;
        [JsonProperty("component")]
        public string Component { get; set; } = ""; // label, expression, upload, textarea, select, switch, checkbox
        [JsonProperty("type")]
        public string Type { get; set; } = ""; //date, time, number, email (http form input types).
        [JsonProperty("isrequired")]
        public bool IsRequired { get; set; } = false;
        [JsonProperty("options")]
        //a simple list of key/value pairs.
        public Options Options { get; set; } = new Options();
        [JsonProperty("dependencies")]
        //a set of conditions and a list of Option references.
        public List<Dependency> Dependencies { get; set; } = new List<Dependency>();
        [JsonProperty("condition")]
        public string Condition { get; set; } = "";
        [JsonProperty("iskey")]
        public bool IsKey { get; set; } = false;
        public override string ToString()
        {
            return Reference;
        }
    }
    public class Option
    {
        [JsonProperty("reference")]
        public string Reference { get; set; } = "";
        [JsonProperty("name")]
        public string Name { get; set; } = "";
        public override string ToString()
        {
            return Reference;
        }

    }
    public class Options : List<Option>
    {
        public Option this[string reference]
        {
            get
            {
                reference = reference.ToUpperInvariant();
                foreach (Option o in this.ToList())
                {
                    if (o.Reference.ToUpperInvariant() == reference) return o;
                }
                return null;
            }
        }
    }
    public class Dependency
    {
        [JsonProperty("condition")]
        //this is a javascript pseudo expression.
        public string Condition { get; set; } = "";
        [JsonProperty("options")]
        //this list refers to the references in the Options collection of a Column
        public List<string> Options { get; set; } = new List<string>();
        public override string ToString()
        {
            return Condition;
        }

    }

}
