﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.SQL;
namespace Core.Foundation
{
    public class Entity
    {
        public Guid ID { get; set; } = Guid.Empty;
        public Guid ClassID { get; set; } = Guid.Empty;
        public string Reference { get; set; } = "";
        public string SearchKey { get; set; } = "";
        public string Name { get; set; } = "";

        public DateTime StartDate { get; set; } = DateTime.MinValue;
        public DateTime EndDate { get; set; } = DateTime.MinValue;
        public string Notes { get; set; } = "";
        public Guid StatusID { get; set; } = Guid.Empty;
        public Guid VariationID { get; set; } = Guid.Empty;
        public int Version { get; set; } = 1;
        public int Quality { get; set; } = 0;
        public double Latitude { get; set; } = 0D; //Y
        public double Longitude { get; set; } = 0D; //X
        public string Locale { get; set; } = "";
        public DateTime DueDate { get; set; } = DateTime.MinValue;
        public DateTime DoneDate { get; set; } = DateTime.MinValue;
        public string URI { get; set; } = "";
        public int PrivilegeLevel { get; set; } = 0;
        public bool IsExternal { get; set; } = false;
        public bool IsVerified { get; set; } = false;
        public int MissionCritical { get; set; } = 0;
        public int DisplayOrder { get; set; } = 0;
        public int Priority { get; set; } = 0;
        public string Phonetics { get; set; } = "";
        public string Password { get; set; } = "";
        public string Owner { get; set; } = "";
        public string Team { get; set; } = "";
        public DateTime RetentionDate { get; set; } = DateTime.MinValue;
        public string W7Alias { get; set; } = "";
        public string ErrorMessage { get; set; } = "";

        //this is a basic save with no business logic.
        private void Save()
        {
            Query q = new Query();
            q.CommandText =
@"EXEC dbo.UpdateEntity
   @id
  ,@classid
  ,@reference
  ,@searchkey
  ,@name
  ,@startdate
  ,@enddate
  ,@notes
  ,@statusid
  ,@version
  ,@quality
  ,@latitude
  ,@longitude
  ,@iconimage
  ,@locale
  ,@duedate
  ,@donedate
  ,@uri
  ,@privilegelevel
  ,@isexternal
  ,@isverified
  ,@variationid
  ,@missioncritical
  ,@displayorder
  ,@priority
  ,@phonetics
  ,@defaultrights
  ,@password
  ,@CRC
  ,@domain
  ,@value
  ,@feedback
  ,@principalname
  ,@teamname
  ,@retentiondate
  ,@w7alias";

            q.Parameter("@id", ID);
            q.Parameter("@classid", ClassID);
            q.Parameter("@reference", Reference);
            q.Parameter("@searchkey", SearchKey);
            q.Parameter("@name", Name);
            q.Parameter("@startdate", StartDate);
            q.Parameter("@enddate", EndDate);
            q.Parameter("@notes", Notes);
            q.Parameter("@statusid", StatusID);
            q.Parameter("@version", Version);
            q.Parameter("@duedate", DueDate);
            q.Parameter("@donedate", DoneDate);
            q.Parameter("@quality", Quality);
            q.Parameter("@latitude", Latitude);
            q.Parameter("@longitude", Longitude);
            q.Parameter("@iconimage", "");
            q.Parameter("@privilegelevel", 0);
            q.Parameter("@locale", Locale);
            q.Parameter("@uri", URI);
            q.Parameter("@isverified", IsVerified);
            q.Parameter("@isexternal", IsExternal);
            q.Parameter("@variationid", VariationID);
            q.Parameter("@missioncritical", MissionCritical);
            q.Parameter("@displayorder", DisplayOrder);
            q.Parameter("@workflowid", -1);
            q.Parameter("@priority", (int)Priority);
            q.Parameter("@phonetics", "");
            q.Parameter("@defaultrights", "");
            q.Parameter("@password", "");
            q.Parameter("@CRC", "");
            q.Parameter("@domain", "");
            q.Parameter("@value", 0);
            q.Parameter("@Feedback", "");
            q.Parameter("@principalname", Owner);
            q.Parameter("@teamname", Team);
            q.Parameter("@retentiondate", RetentionDate);
            q.Parameter("@w7alias", W7Alias);
            q.Select();
            if (!q.EOF)
            {
                ErrorMessage = q.GetString("ErrorMessage");
                if (ErrorMessage != "")
                {
                    throw new Exception(ErrorMessage);
                }
                //log is cleared
                Fill(q);
               
            }
            else
            {
                throw new Exception("The stored procedure UpdateEntity did not return any results.");
            }

            
        }
        public void Fill(Query q)
        {
            //the first table in the resultset contains the main entity and all linked entitys.


            if (q == null) return;
            if (q.EOF) return;

            ID = q.GetGuid("ID");
            //log = q.Changes(id, "Entity");

            ClassID = q.GetGuid("ClassID");
            Reference = q.GetString("Reference");
            SearchKey = q.GetString("SearchKey");            
            Name = q.GetString("Name");
            StartDate = q.GetDate("StartDate");
            EndDate = q.GetDate("EndDate");
            Notes = q.GetString("Notes");        
         
            Version = q.GetInt("Version");
            DueDate = q.GetDate("DueDate");
            DoneDate = q.GetDate("DoneDate");
            Quality = q.GetInt("Quality");
            Latitude = q.GetDouble("Latitude");
            Longitude = q.GetDouble("Longitude");
            Locale = q.GetString("Locale");          
            URI = q.GetString("URI");      
            IsVerified = q.GetBool("IsVerified");
            IsExternal = q.GetBool("IsExternal");
            VariationID = q.GetGuid("VariationID");
            MissionCritical = q.GetInt("MissionCritical");
            DisplayOrder = q.GetInt("DisplayOrder"); 
            Priority = q.GetInt("Priority");    
            Owner = q.GetString("PrincipalName");         
            Team = q.GetString("TeamName");           
            RetentionDate = q.GetDate("retentiondate");            
            W7Alias = q.GetString("w7alias");
           


        }

    }
}
