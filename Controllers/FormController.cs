﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Web;
using Core.SQL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Http.Internal;
using System.Text;
using Core;
using Microsoft.AspNetCore.Cors;
using Core.Forms;
namespace AgileFormsBackEnd.Controllers
{
    [Route("api/[controller]")]
    public class FormController : Controller
    {
        // GET api/values
        [HttpGet]
        
        public IEnumerable<string> Get()
        {
            //this would return an agileform 
            return new string[] { "BFC0DE2018" };
        }

        // GET api/values/5
        [HttpGet("{reference}")]
        
        public string Get(string reference)
        {
            if (System.IO.File.Exists("./forms/" + reference + ".js"))
            {
                return System.IO.File.ReadAllText("./forms/" + reference + ".js");
            }
            return "";

        }

        // POST api/values
        [HttpPost]
        
        public async Task<HttpResponseMessage> Post(object value)
        {
            
           
            try
            {
                //check if it's a multipart message
                string form = Request.Form["JSON"];

                FormFile zip = (FormFile) Request.Form.Files["ZIP"];
                byte[] content = null;
                if (zip != null)
                {
                    //using (Stream s = zip.OpenReadStream())
                    //{

                    //    using (BinaryReader reader = new BinaryReader(s))
                    //    {
                    //        content = reader.ReadBytes(0);
                    //        //content = Encoding.UTF8.GetBytes(await reader.ReadToEndAsync());
                    //    }
                    //}
                    using (MemoryStream m = new MemoryStream())                    {

                        await zip.CopyToAsync(m);
                        content = m.ToArray();
                     }
                }
                Newtonsoft.Json.Linq.JObject json = null;
                json = JObject.Parse(form);
                if (json == null)
                {
                    return new HttpResponseMessage(System.Net.HttpStatusCode.BadRequest);
                }
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };

                FormBase f = JsonConvert.DeserializeObject<FormBase>(form, settings);
                f.Clean(form);
                var found = f.Find("user_email").FirstOrDefault();
                string user_email = found == null ? "" : found;
                

                ConnectionProvider.ConnectionString = "SERVER=.;DATABASE=maatregelen;Integrated Security=SSPI;";
                Query q = new Query();
                q.CommandText = "insert forms (json, body, [user_email], attachment) values (@form, @body, @user_email, @attachment)";
                            
                q.Parameter("@form", form);
                q.Parameter("@body", f.Body);
                q.Parameter("@user_email", user_email);
                q.Parameter("@attachment", content);

                await q.Execute();
            }
            catch(Exception ex)
            {
                return new HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);
            }
            return new HttpResponseMessage(System.Net.HttpStatusCode.OK);


        }

        // PUT api/values/5
        [HttpPut("{reference}")]
        
        public void Put(string reference, [FromBody]string value)
        {
            
        }

        // DELETE api/values/5
        [HttpDelete("{reference}")]
        
        public void Delete(string reference)
        {
        }
       
    }
}
