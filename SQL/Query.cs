﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.ComponentModel;
using System.Globalization;

namespace Core.SQL
{
    /// <summary>
    /// Simplified .NET Core version of the Query object
    /// </summary>
    public class Query
    {
        public event EventHandler QueryCompleted = null;

        SqlConnection connection = new SqlConnection();
        SqlCommand command = new SqlCommand();
        DataSet ds;
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand sqlcommand = new SqlCommand();
        SqlConnection conn;
        int rowindex = 0;

        private static CultureInfo cultureInfo = CultureInfo.CurrentCulture;  
        public Query()
        {
            conn = new SqlConnection(ConnectionProvider.ConnectionString);
            conn.StateChange -= new StateChangeEventHandler(conn_StateChanged);
            conn.StateChange += new StateChangeEventHandler(conn_StateChanged);
            da = new SqlDataAdapter("", conn);
            sqlcommand = new SqlCommand("", conn);          
            newrow.Clear();

        }
        bool connected = false;
        public bool Connected
        {
            get
            {
                return connected;
            }
        }
        public void conn_StateChanged(object o, StateChangeEventArgs e)
        {
            connected = (e.CurrentState == ConnectionState.Open);
        }
        string connectionstring = "";
        public string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(connectionstring))
                {
                    return ConnectionProvider.ConnectionString;
                }
                return connectionstring;
            }
            set
            {
                connectionstring = value;
            }
        }
        /// <summary>
        /// Holds the actual SQL statement
        /// </summary>

        string sql = "";
        public string CommandText
        {
            get
            {
                return sql;
            }
            set
            {
                sqlcommand = new SqlCommand();               
                sqlcommand.Connection = conn;                
                sql = value; //RemoveInjection(value);
                sqlcommand.CommandText = sql;
            }
        }

        public int TableCount
        {
            get
            {
                if (ds != null)
                {
                    return ds.Tables.Count;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Moves to the next rowindex and fills a dictionary of columns.
        /// </summary>
        public void MoveNext()
        {
           
            crazyivan = 0;
            if (Populated())
            {
                if (rowindex + 1 == ds.Tables[currenttable].Rows.Count)
                {
                    eof = true;
                }
                else
                {
                    rowindex++;
                    eof = false;
                    GoTo(rowindex);
                }
            }
        }
        private DataRow dr;
        public void GoTo(int row)
        {
           
            rowindex = row;
            if (ds.Tables[currenttable].Rows.Count > 0)
            {
                dr = ds.Tables[currenttable].Rows[rowindex];
                foreach (DataColumn dc in ds.Tables[currenttable].Columns)
                {
                    newrow[dc.ColumnName] = dr[dc.ColumnName];
                }
                eof = false;

            }
            else
            {
                eof = true;

            }

        }

        public bool Populated()
        {
            return (ds != null && ds.Tables.Count > 0 && ds.Tables[currenttable].Rows.Count > 0);
        }

        public void MovePrevious()
        {
            if (Populated())
            {
                rowindex--;
                if (rowindex < 0)
                {
                    rowindex = 0;
                }
                dr = ds.Tables[currenttable].Rows[rowindex];
            }
        }
        public int CurrentTable
        {
            get
            {
                return currenttable;
            }
            set
            {
                if (ds == null || ds.Tables == null) return;
                if (value < ds.Tables.Count)
                {
                    currenttable = value;
                    GoTop();
                }
                else
                {
                    throw new Exception("Table no. " + value.ToString() + " is not in the resultset.");
                }
            }
        }
        public void GoTop()
        {
            newrow.Clear();
            rowindex = 0;
            crazyivan = 0;
            eof = (ds == null || ds.Tables.Count == 0 || currenttable >= ds.Tables.Count || ds.Tables[currenttable].Rows.Count == 0);
            if (Populated())
            {
                GoTo(rowindex);
            }
        }

        private async Task<bool> Connect()
        {
            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.ConnectionString = ConnectionString;
                    await connection.OpenAsync();

                }
            }
            catch (Exception ex)
            {
                Log.Write(ex);
                return false;
            }
            return true;
        }
        public void Parameter(string name, object value)
        {
            Parameter(name, value.ToString());
        }
        public void Parameter(string name, string value)
        {
            SqlParameter p = command.Parameters.Add(name, SqlDbType.NVarChar, value == null ? 0 : value.Length);
            if (value == null)
            {
                p.Value = DBNull.Value;
            }
            {
                p.Value = value;
            }
        }
        public void Parameter(string name, byte[] value)
        {
            SqlParameter p; 

            if (value == null)
            {
                p = command.Parameters.Add(name, SqlDbType.VarBinary);
                p.Value = DBNull.Value;
            }
            else
            {
                p = command.Parameters.Add(name, SqlDbType.VarBinary, value.Length);
                p.Value = value;
            }
        }
        public void Parameter(string name, int value)
        {
            SqlParameter p = command.Parameters.Add(name, SqlDbType.Int);
            p.Value = value;

        }
        public void Parameter(string name, bool value)
        {
            SqlParameter p = command.Parameters.Add(name, SqlDbType.Bit);
            p.Value = value ? 1 : 0;
        }
        public void Parameter(string name, Guid value)
        {
            SqlParameter p = command.Parameters.Add(name, SqlDbType.UniqueIdentifier);
            p.Value = value;
        }
        public void Parameter(string name, DateTime value)
        {
            SqlParameter p = command.Parameters.Add(name, SqlDbType.DateTime);
            if (value == null)
            {
                p.Value = DBNull.Value;
            }
            else
            {
                DateTime dt = value;
                if (dt == DateTime.MinValue)
                {
                    p.Value = DBNull.Value;
                }
                else
                {
                    p.Value = dt;
                }
            }

        }

        public void Parameter(string name, long value)
        {
            SqlParameter p = command.Parameters.Add(name, System.Data.SqlDbType.BigInt);
            p.Value = value;

        }
        int currenttable = 0;
        public string GetString(string name, string defaultvalue)
        {
            try
            {
                if (name == null) return defaultvalue;
                if (newrow.Count == 0) return defaultvalue;
                if (newrow.ContainsKey(name))
                {
                    object o = GetObject(name, "");
                    if (o is string)
                    {
                        return ((string)o).Trim();
                    }
                    else
                    {
                        return o.ToString().Trim();
                    }
                }
                else
                {
                    return defaultvalue;
                }
            }
            catch (Exception exception)
            {
                Log.Error($"No default value found for {name}. Returning {defaultvalue}.", exception, this);
                return defaultvalue;
            }
        }

        /// <summary>
        /// Gets the value from a column and tries to output it as a string
        /// Returns "" if the conversion fails or the value is null.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns></returns>
        public string GetString(string name)
        {
            return GetString(name, "");
        }

        public object GetObject(string name, object defaultvalue)
        {
            if (newrow == null) return defaultvalue;
            if (!newrow.ContainsKey(name)) return defaultvalue;
            return newrow[name];
        }

        /// <summary>
        /// Gets the value from a column and tries to output it as a byte array
        /// Returns an empty byte[] if the conversion fails or the value is null.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns></returns>
        public byte[] GetBinary(string name)
        {
            byte[] b = new byte[] { };
            try
            {
                if (name == null) return b;
                object columndata = GetObject(name, b);

                if (columndata is byte[])
                {
                    return (Byte[])columndata;
                }
                else
                    return b;
            }
            catch
            {
                //Logger .Info("Query.GetBinary", $"No default value found for {name}. Returning {b}.");
                return b;
            }
        }

        public int GetInt(string name, int defaultValue = 0)
        {
            int? value = GetNullableInt(name);
            return value ?? defaultValue;
        }


        /// <summary>
        /// Gets the value from a column and tries to output it as an int
        /// Returns null if the conversion fails or the value is null.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns></returns>
        public int? GetNullableInt(string name)
        {
            try
            {
                if (name == null) return null;

                object columndata = GetObject(name, 0);
                switch (columndata)
                {
                    case DBNull dBNull:
                        return null;
                    case int i:
                        return i;
                    case string s:
                        if (int.TryParse(s, out int si))
                        {
                            return si;
                        }
                        else
                        {
                            return null;
                        }
                    default:
                        return Convert.ToInt32(columndata);
                }
            }
            catch
            {
                //Logger .Info("Query.GetInt", $"No default value found for {name}. Returning 0.");
                return null;
            }
        }

        /// <summary>
        /// Gets the value from a column and tries to output it as a long number
        /// Returns 0 if the conversion fails or the value is null.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns></returns>
        public long GetLong(string name)
        {
            try
            {
                if (name == null) return 0;

                object columndata = GetObject(name, 0);
                if (columndata is int)
                {
                    long l = (int)columndata;
                    return l;
                }
                if (columndata is long)
                    return (long)columndata;
                else
                {
                    long.TryParse(columndata.ToString(), out long i);
                    //Logger .Info("Query.GetLong", $"Trying to parse {columndata} string. Result {i}.");

                    return i;
                }
            }
            catch
            {
                //Logger .Info("Query.GetLong", $"No default value found for {name}. Returning 0.");
                return 0;
            }
        }

        /// <summary>
        /// Checks if the specified columnname is in the row
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool Contains(string name)
        {
            return (newrow != null && newrow.ContainsKey(name));
        }

        /// <summary>
        /// Returns the name of the database datatype of the column
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string GetDataType(string name)
        {
            int i = 0;
            foreach (DataColumn dc in ds.Tables[currenttable].Columns)
            {
                if (dc.ColumnName.ToUpperInvariant() == name.ToUpperInvariant())
                {
                    return dc.DataType.Name;
                }
                i++;
            }
            return null;
        }

        /// <summary>
        /// Gets the name of the column in the original order of columns
        /// </summary>
        /// <param name="columnindex"></param>
        /// <returns></returns>
        public string GetOrdinalName(int columnindex)
        {
            try
            {
                return ds.Tables[currenttable].Columns[columnindex].ColumnName;
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Checks if the position of the column in the row matches the original order in the database
        /// </summary>
        /// <param name="name">Column name</param>
        /// <param name="columnindex">ordinal column number</param>
        /// <returns></returns>
        public bool IsOrdinal(string name, int columnindex)
        {
            int i = 0;
            foreach (DataColumn dc in ds.Tables[currenttable].Columns)
            {
                if (dc.ColumnName.ToUpperInvariant() == name.ToUpperInvariant() && i == columnindex)
                {
                    return true;
                }

                i++;
            }

            return false;
        }

        /// <summary>
        /// Gets the value from a column and tries to output it as a long / int64 number
        /// Returns 0 if the conversion fails or the value is null.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns></returns>
        public System.Int64 GetInt64(string name)
        {
            try
            {
                if (name == null) return 0;

                object columndata = GetObject(name, 0);
                if (columndata is System.Int64)
                    return (System.Int64)columndata;
                else if (columndata is bool)
                    return (bool)columndata ? 1 : 0;
                else
                {
                    System.Int64.TryParse(columndata.ToString(), out long i);
                    //Logger .Info("Query.GetInt64", $"Trying to parse {columndata} string. Result {i}.");

                    return i;
                }

            }
            catch
            {
                //Logger .Info("Query.GetInt64", $"No default value found for {name}. Returning 0.");
                return 0;
            }
        }

        /// <summary>
        /// Gets the value from a column and tries to output it as a double precision number
        /// Returns 0M if the conversion fails or the value is null.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns></returns>
        public decimal GetDecimal(string name)
        {
            try
            {
                if (name == null) return 0;

                object columndata = GetObject(name, 0M);
                if (columndata == null)
                    return 0M;
                else if (columndata is decimal)
                    return (decimal)columndata;
                else if (columndata is int)
                    return (int)columndata;
                else
                {
                    decimal.TryParse(columndata.ToString(), out decimal d);
                    return d;
                }
            }
            catch
            {
                return 0M;
            }
        }

        /// <summary>
        /// Gets the value from a column and tries to output it as a double precision number
        /// Returns 0D if the conversion fails or the value is null.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns></returns>
        public double GetDouble(string name)
        {
            try
            {
                if (name == null)
                {
                    Log.Warn($"{name} == null.");
                    return 0D;
                }

                object columndata = GetObject(name, 0D);
                if (columndata == null || columndata is DBNull)
                {
                    Log.Debug($"{name}: columndata {columndata} == null.");
                    return 0D;
                }

                switch (columndata)
                {
                    case decimal dec:
                        return (double)dec;
                    case Int16 i16:
                        return i16;
                    case Int32 i32:
                        return i32;
                    case Int64 i64:
                        return i64;
                    case double dob:
                        return dob;
                    case float f:
                        return f;
                    default:
                        if (string.IsNullOrEmpty(columndata.ToString()))
                        {
                            Log.Debug($"{name} is null or empty so return 0");
                            return 0;
                        }
                        else
                        {
                            if (!double.TryParse(columndata.ToString(), NumberStyles.Number, cultureInfo, out double d))
                            {
                                Log.Warn($"Trying to parse {name}({columndata}) string. Result {d}.");
                            }
                            return d;
                        }
                }
            }
            catch (Exception exception)
            {
                Log.Error($"No default value found for {name}. Returning 0D.", exception, this);

                return 0D;
            }
        }

        /// <summary>
        /// Returns the value of a column as a boolean
        /// See GetBool(string name, bool defaultvalue)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool GetBool(string name)
        {
            return GetBool(name, false);
        }

        /// <summary>
        /// Gets the value from a column and tries to output it as a boolean.
        /// Returns a default value the conversion fails or the value is null.
        /// Int values 1 and 0 return true or false.
        /// String values Y J 1 return true, else false
        ///
        /// </summary>
        /// <param name="name">Column name</param>
        /// <param name="defaultvalue">Default value (true or false)</param>
        /// <returns></returns>
        public bool GetBool(string name, bool defaultvalue)
        {
            if (!newrow.ContainsKey(name)) return false;
            object columndata = GetObject(name, false);
            if (columndata is bool)
            {
                return (bool)columndata;
            }
            else if (columndata is int)
            {
                int i = (int)columndata;
                if (i == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else if (columndata is string)
            {
                if (columndata.ToString().ToLowerInvariant() == "J" ||
                    columndata.ToString().ToLowerInvariant() == "Y" ||
                    columndata.ToString().ToLowerInvariant() == "1")
                    return true;
                else
                    return false;
            }
            else
            {
                //Logger .Info("Query.GetBool", $"No default value found for {name}. Returning {defaultvalue}.");
                return defaultvalue;
            }
        }

      
        /// <summary>
        /// Gets the value from a column and tries to output it as a globally unique identifier (GUID) object.
        /// Returns empty GUID if the conversion fails or the value is null.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns></returns>
        public Guid GetGuid(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Guid.Empty;
            }

            object columndata = GetObject(name, Guid.Empty);

            if (columndata is Guid)
            {
                return (Guid)columndata;
            }

            if (columndata is string)
            {
                Guid result = Guid.Empty;
                if (Guid.TryParse((string)columndata, out result))
                {
                    //Logger .Info("Query.GetGuid", $"No default value found for {name}. Returning {result}.");
                    return result;
                }
            }

            //Logger .Info("Query.GetGuid", $"No default value found for {name}. Returning {Guid.Empty}.");
            return Guid.Empty;
        }

        public override string ToString()
        {
            if (TableCount == 1)
            {
                return string.Format("{0} rows", Count);
            }
            else
            {
                return string.Format("{0} rows, {1}/{2} tables", Count, CurrentTable + 1, TableCount);
            }
        }

        public Guid? GetNullableGuid(string name)
        {
            var result = GetGuid(name);

            return result == Guid.Empty ? (Guid?)null : result;
        }

        /// <summary>
        /// Gets the value from a column and tries to output it as a DateTime object.
        /// Returns DateTime.Minvalue if the conversion fails or the database date is null.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns></returns>
        public DateTime GetDate(string name)
        {
            try
            {
                if (name == null) return DateTime.MinValue;
                object columndata = GetObject(name, DateTime.MinValue);
                if (columndata is DateTime)
                {
                    return (DateTime)columndata;
                }
                else
                {
                    DateTime dt = DateTime.MinValue;
                    DateTime.TryParse(columndata.ToString(), out dt);
                    return dt;
                }

            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        public int Count
        {
            get
            {
                if (Populated())
                {
                    return ds.Tables[currenttable].Rows.Count;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the value from a column and tries to output it as a DateTime object.
        /// Returns DateTime.Minvalue if the conversion fails or the database date is null.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <returns></returns>
        public DateTime? GetNullableDate(string name)
        {
            try
            {
                if (name == null) return DateTime.MinValue;
                object columndata = GetObject(name, DateTime.MinValue);
                if (columndata is DateTime)
                {
                    return (DateTime)columndata;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }
        public async Task Execute()
        {
            if (await Connect())
            {
                command.Connection = connection;
                command.CommandText = sql;
                await command.ExecuteNonQueryAsync();
            }
        }
        bool IsWorking { get; set; } = false;
        string ErrorMessage { get; set; } = "";
        private SortedDictionary<string, object> newrow = new SortedDictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);


        int crazyivan = 0;
        bool eof = true;
        public bool EOF
        {
            get
            {
                
                //just in case somebody forgets to move next (like me).
                crazyivan++;
                if (crazyivan > 10)
                {
                    throw new Exception("SQL class: Please use MoveNext() to avoid endless loops.");
                }
                return eof;
            }
        }
       

        public void Select()
        {
           
            newrow.Clear();
            ErrorMessage = "";

            ds = new DataSet();
            if (string.IsNullOrEmpty(sql)) return;            
            da.SelectCommand = sqlcommand;            
            da.Fill(ds);
            if (ds == null)
            {
                rowindex = -1;
                eof = true;
                currenttable = -1;
            }
            else
            {
                currenttable = 0;
                GoTop();
                ErrorMessage = GetString("ErrorMessage");
            }

            QueryCompleted?.Invoke(this, EventArgs.Empty);
            return;
        }

    }
    public static class ConnectionProvider
    {


        /// <summary>
        /// Sets the connectionstring 
        /// </summary>
        public static string ConnectionString { get; set; }
        /// <summary>
        /// Tests if the SQLServer is present
        /// </summary>
        /// <returns></returns>
        public static bool Test()
        {
            return false;
        }
    }
}
